/**
 * Created on 25.06.2019.
 */
//Обработчик событий - это по сути описание функции, которая должна выполняться при взаимодействии с элементами страницы

const price = document.getElementById('price-input'),
    warning = document.createElement('span'),
    finalPrice = document.createElement('span'),
    closeButton = document.createElement('button'),
    closeButtonImg = document.createElement('img');

price.addEventListener('focus', function makeGreenBorder(){
    this.style.borderColor = 'rgba(18, 188, 41, .7'
});

price.addEventListener('blur', function (){
    this.style.borderColor = '';
    validation(this);
});
function validation(price) {

    if(parseFloat(price.value)<0 || isNaN (parseFloat(price.value))){
        finalPrice.remove();
        price.style.backgroundColor='rgba(249, 162, 168, .5)';
        document.querySelector('label').appendChild(warning);
        warning.innerText="Please enter correct price!";
        warning.classList.add('warning-text');
}else{

      warning.remove();
      price.style.backgroundColor='rgba(151, 251, 165, .5)';
      document.querySelector('label').appendChild(finalPrice);
      finalPrice.innerText=`Current price: ${parseFloat(price.value)}$`;
      finalPrice.classList.add('final-price');
      finalPrice.appendChild(closeButton);
      closeButton.classList.add('close');
      closeButton.appendChild(closeButtonImg);
      closeButtonImg.setAttribute("src", "img/close.png");
      closeButtonImg.setAttribute("width", "16px");
      closeButton.addEventListener('click',()=>{
          finalPrice.remove();
          price.value = '';
          price.style.backgroundColor='';
      })
    }

}

